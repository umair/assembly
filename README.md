# Pakistan Assembly API
---
An API to get list of MNAs, MPAs in Pakistan assembly. 

## Setup

Follow the steps below to run it on your machine.
1. **git clone** https://umair@bitbucket.org/umair/assembly.git
2. **python -m venv** venv to create a virtual environment for this API
3. **venv\Scripts\activate** on Windows or **source venv/bin/activate** on mac/linux
4. **pip install -r requirements.txt** to install dependencies.
5. **python manage.py runserver** to run the webserver and get the API url.
6. Copy the API url and open in any browser to play with RESTful API.

## Docker Setup

Follow the steps below to run it on your docker.
1. **docker-compose up -d**
2. **docker-compose down** to stop
---
## Run Tests
python manage.py test members