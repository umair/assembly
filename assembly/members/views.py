from rest_framework import viewsets
from .models import Member
from .serializers import MemberSerializer


class MemberViewset(viewsets.ModelViewSet):
    serializer_class = MemberSerializer

    def get_queryset(self):
        queryset = Member.objects.all()
        # add filters here is required.
        return queryset

