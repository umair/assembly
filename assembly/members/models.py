from django.db import models


class Member(models.Model):
    name = models.CharField(max_length=50, blank=False)
    assembly = models.CharField(max_length=20, blank=False)  # NATIONAL, PUNJAB, SINDH, KPK, BALOCHISTAN
    constituency = models.CharField(max_length=15, blank=False)