from django.test import TestCase
from .models import Member


class MemberTest(TestCase):
    def setUp(self):
        Member.objects.create(name="Awais", assembly="NATIONAL", constituency="NA-63")

    def test_check_constituency_by_name(self):
        m = Member.objects.get(name="Awais")
        self.assertEqual(m.constituency, "NA-63")
        # a method can be created and then it can be mocked
        # I am not mocking I am using default functionality by the framework
        # so, it becomes a kind of integration test.