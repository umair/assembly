from django.urls import include, path
from rest_framework import routers
from .views import MemberViewset

router = routers.DefaultRouter()
router.register('members', MemberViewset, 'member')

urlpatterns = router.urls