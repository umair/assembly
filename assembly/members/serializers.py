from .models import Member
from rest_framework import serializers


class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = '__all__'

    name = serializers.CharField(required=True)
    assembly = serializers.CharField(required=True)
    constituency = serializers.CharField(required=True)
